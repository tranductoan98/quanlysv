import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Student } from '../models/students';
import { CommonService } from '../Service/common.service';
import { ServerHttpService } from '../Service/server-http.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  students: Student[]= [];

  constructor(
    private commo: CommonService,
    private serverHttp: ServerHttpService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadData()
  }
  addStudent(){
    this.router.navigate(['studentform', 0]);
  }
  deleteStudent(studentId: any) {
    this.serverHttp.deleteStudent(studentId).subscribe((data) => {
      console.log('delete', data);
      this.loadData();
    });
  }
  loadData() {
    this.serverHttp.getStudents().subscribe((data) => {
      console.log('getStudents', data);
      this.students = data;
    });
  }
  public editStudent(studentId: any) {
    this.router.navigate(['studentform', studentId]);
  }
}
