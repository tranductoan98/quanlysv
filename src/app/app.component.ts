import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav/sidenav';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'QLSV';
  @ViewChild('sidenav')
  sidenav!: MatSidenav;
  isOpened = false;



  public openLeftSide() {
    this.isOpened = !this.isOpened;
    this.sidenav.toggle();
  }

  public closeLeftSide() {
    this.isOpened = false;
  }
}
